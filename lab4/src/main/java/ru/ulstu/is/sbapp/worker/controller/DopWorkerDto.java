package ru.ulstu.is.sbapp.worker.controller;

public class DopWorkerDto {
    public String firstName;
    public String lastName;
    public Long city;
    public String serial;

    public DopWorkerDto() {

    }

    public DopWorkerDto(String firstName, String lastName, Long city, String serial) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.serial = serial;
    }
}
