package ru.ulstu.is.sbapp.worker.controller;

import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;

import java.util.List;

public class CityDto {
    private long id;
    private String name;
    private String workers="";

    public CityDto(){}

    public CityDto(City city) {
        this.id = city.getId();
        this.name = city.getName();
        if(city.getWorkers()!=null)
            this.workers=city.getWorkers().toString();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWorkers() {
        return workers;
    }
}
