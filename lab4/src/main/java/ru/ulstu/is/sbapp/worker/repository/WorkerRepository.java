package ru.ulstu.is.sbapp.worker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;

import java.util.List;

public interface WorkerRepository extends JpaRepository<Worker, Long> {
    @Query("SELECT w From Worker w Where w.city.id = :city and w.passport.serial LIKE %:serial%")
    List<Worker> findByNameContaining(@Param("city")Long city, @Param("serial")String serial);
}
