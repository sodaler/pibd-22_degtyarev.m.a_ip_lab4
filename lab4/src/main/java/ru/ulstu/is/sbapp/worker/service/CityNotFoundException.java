package ru.ulstu.is.sbapp.worker.service;

public class CityNotFoundException extends RuntimeException {
    public CityNotFoundException(Long id) {
        super(String.format("City with id [%s] is not found", id));
    }
}
