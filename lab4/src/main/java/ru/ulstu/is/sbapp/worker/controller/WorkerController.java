package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.service.WorkerService;
import ru.ulstu.is.sbapp.test.model.TestDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/worker")
public class WorkerController {
    private final WorkerService workerService;

    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("/{id}")
    public WorkerDto getWorker(@PathVariable Long id) {
        return new WorkerDto(workerService.findWorker(id));
    }

    @GetMapping("/")
    public List<WorkerDto> getWorkers() {
        return workerService.findAllWorkers().stream()
                .map(WorkerDto::new)
                .toList();
    }

    @PostMapping("/")
    public WorkerDto createWorker(@RequestParam("firstName") String firstName,
                                    @RequestParam("lastName") String lastName,
                                    @RequestParam("serial") String serial,
                                    @RequestParam("number") String number,
                                    @RequestParam("city") Long city) {
        return new WorkerDto(workerService.addWorker(firstName, lastName, serial, number, city));
    }
    
    @PutMapping("/{id}")
    public WorkerDto updateWorker(@RequestBody @Valid WorkerDto workerDto) {
        return workerService.updateWorker(workerDto);
    }

    @DeleteMapping("/{id}")
    public WorkerDto deleteWorker(@PathVariable Long id) {
        return new WorkerDto(workerService.deleteWorker(id));
    }

    @GetMapping("/check")
    public List<WorkerDto> getWorkers(@RequestParam("city") Long name, @RequestParam("serial") String serial) {
      return workerService.findByNameContaining(name, serial);
    }
}