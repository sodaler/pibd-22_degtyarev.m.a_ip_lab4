package ru.ulstu.is.sbapp.worker.controller;

import ru.ulstu.is.sbapp.worker.model.Worker;

public class WorkerDto {
    private long id;
    private String firstName;
    private String lastName;
    private String serial;
    private String number;
    private Long city;

    public WorkerDto(Worker worker) {
        this.id = worker.getId();
        this.firstName=worker.getFirstName();
        this.lastName=worker.getLastName();
        this.serial=worker.getPassport().getSerial();
        this.number=worker.getPassport().getNumber();
        this.city= worker.getCity().getId();
    }

    public WorkerDto() {
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSerial() {
        return serial;
    }

    public String getNumber() {
        return number;
    }

    public Long getCity() {
        return city;
    }
}
